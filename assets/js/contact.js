$('#di-contact-form').submit(function(event){
    var data = $(this).serializeArray();
    event.preventDefault();
    $('.dealerInspireErrors').html('');
    $.ajax({
        method: "POST",
        url: 'contact.php',
        dataType: 'json',
        data: data
    }).done(function(response){
        if(response.success === 'success'){
            $('#di-contact-form').slideUp('fast',function(){
                $('.di-contact-success').slideDown();
            });
        }else if(response.errors){
            console.log('errors', response.errors);
            $.each(response.errors, function(key,value){
                console.log('kv', capitalize(key), value);
                $('#dealerInspire' + capitalize(key) + 'Errors').append('<li>' + value + '</li>');
            });
        }
    });
});

function capitalize(string) {
    return string[0].toUpperCase() + string.slice(1);
}