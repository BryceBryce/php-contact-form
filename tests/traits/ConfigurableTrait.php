<?php
/**
 * Created by PhpStorm.
 * User: Reelworld-3
 * Date: 8/5/2018
 * Time: 9:04 PM
 */

namespace Tests\traits;

use App\Classes\Contact;
use PDO;
use App\Classes\Database;
use App\Classes\Mailer;
use Swift_Mailer;
use Faker\Factory;

trait ConfigurableTrait
{
    /**
     * Database name preceded by test_
     *
     * @return string
     */
    private function databaseForTesting(){
        return 'test_' . ($_ENV['DB_DATABASE'] ?: 'contact');
    }

    /**
     * Table name preceded by test_
     *
     * @return string
     */
    private function tableForTesting(){
        return 'test_' . ($_ENV['DB_TABLE'] ?: 'contact_submissions');
    }

    /**
     * Gets connection to database and drops test table
     *
     * @return PDO
     */
    private function getFreshDatabase(){
        $pdo = $this->instantiatePDO();
        $database = $this->databaseForTesting();
        $table = $this->tableForTesting();
        $preparedPDO = $pdo->prepare("USE $database");
        $preparedPDO->execute();
        $preparedPDO = $pdo->prepare("DROP TABLE IF EXISTS $table");
        $preparedPDO->execute();
        return $pdo;
    }

    /**
     * Valid test data
     *
     * @return array
     */
    private function validTestData(){
        $faker = Factory::create();
        return [
            'email' => $faker->email,
            'message' => $faker->sentence(5),
            'name' => $faker->name,
            'phone' => $faker->phoneNumber
        ];
    }

    /**
     * Test configured Contact
     *
     * @return Contact
     */
    private function instantiateContact(){
        return new Contact($this->instantiateMailer(), $this->instantiateDatabase());
    }

    /**
     * Test configured Database
     *
     * @return Database
     */
    private function instantiateDatabase(){
        return new Database($this->instantiatePDO(), $this->databaseForTesting(), $this->tableForTesting());
    }

    /**
     * Test configured Mailer
     *
     * @return Mailer
     */
    private function instantiateMailer(){
        return new Mailer($this->instantiateSwiftMailer(), $_ENV['MAIL_TO'] ?: 'guy-smiley@example.com ');
    }

    /**
     * Mock Swift_Mailer with overridden send method
     *
     * @return Swift_Mailer
     */
    private function instantiateSwiftMailer(){
        $mockSwiftMailer = $this->createMock(\Swift_Mailer::class);
        $mockSwiftMailer->expects($this->any())
            ->method('send')
            ->will($this->returnCallback(function (\Swift_Message $message) {
                return $message;
            }));
        return $mockSwiftMailer;
    }

    /**
     * PDO with connection to MySQL server, but no database
     *
     * @return PDO
     */
    private function instantiatePDO(){
        $host = $_ENV['DB_HOST'];
        $dsn = "mysql:host=$host";
        $username = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];
        try {
            return new PDO($dsn, $username, $password);
        } catch (\PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }
}