<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Tests\traits\ConfigurableTrait;

final class ContactTest extends TestCase
{
    use ConfigurableTrait;

    /**
     * @test
     */
    public function successReturnedWithValidData(): void
    {
        $testData = $this->validTestData();
        $contact = $this->instantiateContact();
        $result = $contact->processForm($testData);
        $this->assertTrue(array_key_exists('success', $result));
        $this->assertTrue(in_array('success', $result));
    }

    /**
     * @test
     */
    public function emailIsRequiredErrorCreatedForMissingEmail(): void
    {
        $testData = $this->validTestData();
        unset($testData['email']);
        $contact = $this->instantiateContact();
        $contact->processForm($testData);
        $this->assertEquals($contact->getErrors()['email'], 'Email is required.');
        $this->assertTrue($contact->hasErrors());
    }

    /**
     * @test
     */
    public function emailFailedValidationErrorCreatedForMalformedEmail(): void
    {
        $testData = $this->validTestData();
        $testData['email'] = 'this is malformed email';
        $contact = $this->instantiateContact();
        $contact->processForm($testData);
        $this->assertEquals($contact->getErrors()['email'], 'Email failed validation.');
        $this->assertTrue($contact->hasErrors());
    }

    /**
     * @test
     */
    public function nameIsRequiredErrorCreatedForMissingEmail(): void
    {
        $testData = $this->validTestData();
        unset($testData['name']);
        $contact = $this->instantiateContact();
        $contact->processForm($testData);
        $this->assertEquals($contact->getErrors()['name'], 'Name is required.');
        $this->assertTrue($contact->hasErrors());
    }

    /**
     * @test
     */
    public function messageIsRequiredErrorCreatedForMissingMessage(): void
    {
        $testData = $this->validTestData();
        unset($testData['message']);
        $contact = $this->instantiateContact();
        $contact->processForm($testData);
        $this->assertEquals($contact->getErrors()['message'], 'Message is required.');
        $this->assertTrue($contact->hasErrors());
    }

    /**
     * @test
     */
    public function noErrorsCreatedForMissingPhone(): void
    {
        $testData = $this->validTestData();
        unset($testData['phone']);
        $contact = $this->instantiateContact();
        $contact->processForm($testData);
        $this->assertEquals(false, $contact->hasErrors());
        $this->assertFalse($contact->hasErrors());
    }
}