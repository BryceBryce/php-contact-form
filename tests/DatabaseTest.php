<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Tests\traits\ConfigurableTrait;

final class DatabaseTest extends TestCase
{
    use ConfigurableTrait;

    /**
     * @test
     */
    public function createsDatabaseOnInstantiation(): void
    {
        $pdo = $this->instantiatePDO();
        $database = $this->databaseForTesting();
        $preparedPDO = $pdo->prepare("DROP DATABASE $database");
        $preparedPDO->execute();
        $preparedPDO = $pdo->prepare("USE $database");
        $this->assertEquals(false, $preparedPDO->execute());
        $this->instantiateDatabase();
        $preparedPDO = $pdo->prepare("USE $database");
        $this->assertEquals(true, $preparedPDO->execute());
    }

    /**
     * @test
     */
    public function createsTableOnInstantiation(): void
    {
        $pdo = $this->getFreshDatabase();
        $table = $this->tableForTesting();
        $preparedPDO = $pdo->prepare("SELECT 1 FROM $table");
        $this->assertEquals(false, $preparedPDO->execute());
        $this->instantiateDatabase();
        $preparedPDO = $pdo->prepare("SELECT 1 FROM $table");
        $this->assertEquals(true, $preparedPDO->execute());
    }

    /**
     * @test
     */
    public function storeSucceedsWithPhoneInDatabase(): void
    {
        $testData = $this->validTestData();
        $pdo = $this->getFreshDatabase();
        $table = $this->tableForTesting();
        $database = $this->instantiateDatabase();
        $preparedPDO = $pdo->prepare("SELECT COUNT(id) FROM $table");
        $preparedPDO->execute();
        $this->assertEquals(0, $preparedPDO->fetchColumn());
        $this->assertTrue($database->store($testData));
        $preparedPDO = $pdo->prepare("SELECT COUNT(id) FROM $table");
        $preparedPDO->execute();
        $this->assertEquals(1, $preparedPDO->fetchColumn());
        $preparedPDO = $pdo->prepare("SELECT email, message, name, phone FROM $table LIMIT 1");
        $preparedPDO->execute();
        $result = $preparedPDO->fetch();
        foreach(array_keys($testData) as $key){
            $this->assertEquals($result[$key], $testData[$key]);
        }
    }

    /**
     * @test
     */
    public function storeSucceedsWithNoPhoneInDatabase(): void
    {
        $testData = $this->validTestData();
        unset($testData['phone']);
        $pdo = $this->getFreshDatabase();
        $table = $this->tableForTesting();
        $database = $this->instantiateDatabase();
        $preparedPDO = $pdo->prepare("SELECT COUNT(id) FROM $table");
        $preparedPDO->execute();
        $this->assertEquals(0, $preparedPDO->fetchColumn());
        $this->assertTrue($database->store($testData));
        $preparedPDO = $pdo->prepare("SELECT COUNT(id) FROM $table");
        $preparedPDO->execute();
        $this->assertEquals(1, $preparedPDO->fetchColumn());
        $preparedPDO = $pdo->prepare("SELECT email, message, name, phone FROM $table LIMIT 1");
        $preparedPDO->execute();
        $result = $preparedPDO->fetch();
        foreach(array_keys($testData) as $key){
            $this->assertEquals($result[$key], $testData[$key]);
        }
    }

    /**
     * @test
     */
    public function storeFailsAndReturnsFalseWithNoEmail(): void
    {
        $testData = $this->validTestData();
        unset($testData['email']);
        $pdo = $this->getFreshDatabase();
        $table = $this->tableForTesting();
        $database = $this->instantiateDatabase();
        $this->assertFalse($database->store($testData));
        $preparedPDO = $pdo->prepare("SELECT COUNT(id) FROM $table");
        $preparedPDO->execute();
        $this->assertEquals(0, $preparedPDO->fetchColumn());
    }

    /**
     * @test
     */
    public function storeFailsAndReturnsFalseWithNoName(): void
    {
        $testData = $this->validTestData();
        unset($testData['name']);
        $pdo = $this->getFreshDatabase();
        $table = $this->tableForTesting();
        $database = $this->instantiateDatabase();
        $this->assertFalse($database->store($testData));
        $preparedPDO = $pdo->prepare("SELECT COUNT(id) FROM $table");
        $preparedPDO->execute();
        $this->assertEquals(0, $preparedPDO->fetchColumn());
    }

    /**
     * @test
     */
    public function storeFailsAndReturnsFalseWithNoMessage(): void
    {
        $testData = $this->validTestData();
        unset($testData['message']);
        $pdo = $this->getFreshDatabase();
        $table = $this->tableForTesting();
        $database = $this->instantiateDatabase();
        $this->assertFalse($database->store($testData));
        $preparedPDO = $pdo->prepare("SELECT COUNT(id) FROM $table");
        $preparedPDO->execute();
        $this->assertEquals(0, $preparedPDO->fetchColumn());
    }
}