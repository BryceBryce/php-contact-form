<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Tests\traits\ConfigurableTrait;

final class MailerTest extends TestCase
{
    use ConfigurableTrait;

    /**
     * @test
     */
    public function correctlySetsBodyOnMessage(): void
    {
        $testData = $this->validTestData();
        $mailer = $this->instantiateMailer();
        $mailer->setBody($testData['message']);
        $this->assertEquals($testData['message'], $mailer->getMessage()->getBody());
    }

    /**
     * @test
     */
    public function correctlySetsFromWithEmailOnlyOnMessage(): void
    {
        $testData = $this->validTestData();
        $mailer = $this->instantiateMailer();
        $mailer->setFrom($testData['email']);
        $this->assertTrue(array_key_exists($testData['email'], $mailer->getMessage()->getFrom()));
    }

    /**
     * @test
     */
    public function correctlySetsFromWithEmailAndNameOnMessage(): void
    {
        $testData = $this->validTestData();
        $mailer = $this->instantiateMailer();
        $mailer->setFrom($testData['email'], $testData['name']);
        $this->assertTrue(array_key_exists($testData['email'], $mailer->getMessage()->getFrom()));
        $this->assertTrue(in_array($testData['name'], $mailer->getMessage()->getFrom()));
    }

    /**
     * @test
     */
    public function sentMessageHasCorrectValues(): void
    {
        $testData = $this->validTestData();
        $mailer = $this->instantiateMailer();
        $mailer->setFrom($testData['email'], $testData['name']);
        $mailer->setBody($testData['message']);
        $sent = $mailer->send();
        $this->assertTrue(array_key_exists($mailer->getTo(), $sent->getTo()));
        $this->assertTrue(array_key_exists($testData['email'], $sent->getFrom()));
        $this->assertTrue(in_array($testData['name'], $sent->getFrom()));
        $this->assertEquals($testData['message'], $sent->getBody());
    }
}