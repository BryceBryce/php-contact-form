<?php
/**
 * Created by PhpStorm.
 * User: Reelworld-3
 * Date: 8/6/2018
 * Time: 9:21 PM
 */

namespace App\Providers;

use App\Classes\Application;

abstract class ServiceProvider
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function register()
    {

    }

    public function boot()
    {

    }
}