<?php

namespace App\Providers;

use App\Classes\Database;
use PDO;

class DatabaseProvider extends ServiceProvider
{
    public function register(){
        $config = $this->app->getConfig('database');
        $host = $config['host'];
        $dsn = "mysql:host=$host";
        $username = $config['username'];;
        $password = $config['password'];
        try {
            $pdo = new PDO($dsn, $username, $password);
            $database = new Database($pdo,$config['database'], $config['table']);
            $this->app->instance('database', $database);
        } catch (\PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }
}