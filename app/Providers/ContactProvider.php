<?php

namespace App\Providers;


use App\Classes\Contact;

class ContactProvider extends ServiceProvider
{
    public function boot(){
        $mailer = $this->app->getInstance('mailer');
        $database = $this->app->getInstance('database');
        $contact = new Contact($mailer, $database);
        $this->app->instance('contact', $contact);
    }
}