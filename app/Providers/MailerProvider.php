<?php

namespace App\Providers;

use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use App\Classes\Mailer;

class MailerProvider extends ServiceProvider
{
    public function register(){
        $config = $this->app->getConfig('mail');
        $transport = (new Swift_SmtpTransport($config['host'], $config['port']))
            ->setUsername($config['username'])
            ->setPassword($config['password'])
        ;
        $swiftMailer = new Swift_Mailer($transport);
        $mailer = new Mailer($swiftMailer, $config['to']);
        $this->app->instance('mailer', $mailer);
    }
}