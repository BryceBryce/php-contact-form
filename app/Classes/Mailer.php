<?php

namespace App\Classes;

use Swift_Mailer;
use Swift_Message;

class Mailer
{
    private $mailer;
    private $message;
    private $to;

    public function __construct(Swift_Mailer $mailer, $to){
        $this->mailer = $mailer;
        $this->to = $to;
        $this->message = new Swift_Message('Guy Smiley Contact Form');
        $this->message->setTo($this->to);
    }

    /**
     * Sets body of email
     *
     * @param  string  $body
     * @return Mailer
     */
    public function setBody($body){
        $this->message->setBody($body, 'text/html');
        return $this;
    }

    /**
     * Sets from of email
     *
     * @param  string  $email
     * @param  string  $name
     * @return Mailer
     */
    public function setFrom($email, $name = null)
    {
        $from = $email;
        if($name !== null){
            $from = [$email => $name];
        }
        $this->message->setFrom($from);
        return $this;
    }

    /**
     * Gets message
     *
     * @return Swift_Message
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * Gets to address
     *
     * @return string
     */
    public function getTo(){
        return $this->to;
    }

    /**
     * Sends current message
     *
     * @return mixed
     */
    public function send(){
        return $this->mailer->send($this->message);
    }


}