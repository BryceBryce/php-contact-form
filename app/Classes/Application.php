<?php

namespace App\Classes;

use App\Providers\ServiceProvider;
use Exception;

class Application
{

    protected $config;
    protected $instances;
    private $providers;


    /**
     * Creates the Application
     *
     * @throws Exception if class is in providers config, but the class does not exist
     * @throws Exception if class is in providers config is not an instance of App\Providers\ServiceProvider
     */
    public function __construct($config)
    {
        $this->config = $config;

        $this->createProviders();

        $this->register();

        $this->boot();
    }

    /**
     * Creates Providers and adds them to the Application
     *
     * @throws Exception if class is in providers config, but the class does not exist
     * @throws Exception if class is in providers config is not an instance of App\Providers\ServiceProvider
     */
    private function createProviders()
    {
        foreach($this->config['providers'] as $provider){
            if(class_exists($provider)){
                $providerInstance = new $provider($this);
                if(!$providerInstance instanceof ServiceProvider){
                    throw new Exception($provider ." is configured as a Provider, but is not an instance of App\Providers\ServiceProvider");
                }
                $this->providers[] = $providerInstance;
            }else{
                throw new Exception($provider . ' is in config, but does not exists');
            }
        }
    }

    /**
     * Calls register on all Providers
     */
    private function register()
    {
        foreach($this->providers as $provider){
            $provider->register();
        }
    }

    /**
     * Calls boot on all Providers
     */
    private function boot()
    {
        foreach($this->providers as $provider){
            $provider->boot();
        }
    }

    /**
     * Adds an instance to Application instances
     *
     * @param  string  $name
     * @return  string|array
     */
    public function getConfig($name)
    {
        if(isset($this->config[$name])){
            return $this->config[$name];
        }
    }

    /**
     * Adds an instance to Application instances
     *
     * @param  string  $name
     * @param  mixed  $instance
     */
    public function instance($name, $instance)
    {
        $this->instances[$name] = $instance;
    }

    /**
     * Get Instance from Application
     *
     * @param  string  $name
     * @return mixed
     */
    public function getInstance($name){
        if(isset($this->instances[$name])){
            return $this->instances[$name];
        }
    }
}