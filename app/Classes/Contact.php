<?php

namespace App\Classes;

use App\Classes\Database;
use App\Classes\Mailer;

class Contact
{
    const RULES = [
        'email' => ['required' => true, 'filter' => FILTER_VALIDATE_EMAIL],
        'name' => ['required' => true],
        'message' => ['required' => true]
    ];
    private $mailer;
    private $database;
    private $form;
    private $errors;

    public function __construct(Mailer $mailer, Database $database)
    {
        $this->mailer = $mailer;
        $this->database = $database;
    }

    /**
     * Processes form by validating
     *
     * @param  array  $form
     * @return array
     */
    public function processForm(array $form)
    {
        $this->form['email'] = $form['email'] ?? false;
        $this->form['name'] = $form['name'] ?? false;
        $this->form['message'] = $form['message'] ?? false;
        $this->form['phone'] = $form['phone'] ?? false;

        if($this->validateForm()){
            $this->sendEmail();
            $this->saveToDatabase();
        }

        return $this->generateResponse();
    }

    /**
     * Validates form data based on RULES const
     *
     * @return bool
     */
    private function validateForm()
    {
        foreach(self::RULES as $key => $rules){
            if($rules['required'] && !$this->form[$key]){
                $this->errors[$key] = ucfirst($key) . ' is required.';
            }elseif(isset($rules['filter']) && !filter_var($this->form[$key], $rules['filter'])){
                $this->errors[$key] = ucfirst($key) . ' failed validation.';
            }
        }
        return !$this->hasErrors();
    }

    /**
     * App\Classes\Mailer email sending
     */
    private function sendEmail()
    {
        $this->mailer->setFrom($this->form['email'], $this->form['name'])
                ->setBody($this->buildBody())
                ->send();
    }

    /**
     * App\Classes\Database store method
     */
    private function saveToDatabase()
    {
        $this->database->store($this->form);
    }

    /**
     * Returns success array when there are no errors, or returns errors array
     *
     * @return array
     */
    private function generateResponse()
    {
        if(!$this->hasErrors()){
            return ['success' => 'success'];
        }
        return ['errors' => $this->errors];
    }

    /**
     * Indicates if there are errors
     *
     * @return bool
     */
    public function hasErrors(){
        return $this->errors !== NULL;
    }

    /**
     * Return contact errors
     *
     * @return array|null
     */
    public function getErrors(){
        return $this->errors;
    }

    /**
     * Simple string built email template
     *
     * @return string
     */
    private function buildBody(){
        $body = '<html><body>';
        $body .= '<h1>Guy Smiley Contacted</h1>';
        foreach($this->form as $key => $value){
            if($value){
                $formattedKey = ucfirst($key);
                $body .= "<div>$formattedKey: $value</div>";
            }
        }
        $body .= '</body></html>';
        return $body;
    }
}