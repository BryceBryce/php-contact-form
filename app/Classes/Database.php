<?php

namespace App\Classes;

use PDO;

class Database
{
    private $pdo;
    private $database;
    private $table;

    public function __construct(PDO $pdo, $database, $table){
        $this->pdo = $pdo;
        $this->database = $database;
        $this->table = $table;
        $this->findOrCreateDatabase();
        $this->findOrCreateTable();
    }

    /**
     * Builds insert query dynamically from array. Excludes falsey values
     *
     * @param  array  $data
     * @return bool
     */
    public function store(array $data){
        $preparedData = [];
        foreach($data as $key => $value){
            if($value){
                $preparedData[":$key"] = $value;
            }
        }
        $preparedKeys = implode(',',array_keys($preparedData));
        $keys = str_replace(':', '', $preparedKeys);
        $preparedPDO =  $this->pdo->prepare("INSERT INTO $this->table ($keys) VALUES ($preparedKeys)");
        return $preparedPDO->execute($preparedData);
    }

    /**
     * Creates table if it doesn't exists. Saved tester from having to create table at the cost of performance
     */
    private function findOrCreateTable(){
        $preparedPDO = $this->pdo->prepare("SELECT 1 FROM $this->table");
        $results = $preparedPDO->execute();
        if(!$results){
            $this->createTable();
        }
    }

    /**
     * Creates database if it doesn't exists. Saved tester from having to create database at the cost of performance
     */
    private function findOrCreateDatabase(){
        $preparedPDO = $this->pdo->prepare("USE $this->database");
        $results = $preparedPDO->execute();
        if(!$results){
            $preparedPDO = $this->pdo->prepare("CREATE DATABASE $this->database");
            $preparedPDO->execute();
        }
        $preparedPDO = $this->pdo->prepare("USE $this->database");
        $preparedPDO->execute();
    }

    /**
     * Creates table for contact form
     */
    private function createTable(){
        $sql = "CREATE TABLE $this->table (
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            name VARCHAR(30) NOT NULL,
            message text NOT NULL,
            email VARCHAR(70) NOT NULL,
            phone VARCHAR(50)
        )";
        $preparedPDO = $this->pdo->prepare($sql);
        $preparedPDO->execute();
    }

}