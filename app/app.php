<?php
require dirname(__DIR__) . '/vendor/autoload.php';

use App\Classes\Application;

$config = require 'config.php';

return new Application($config);