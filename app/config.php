<?php

use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load( dirname(__DIR__) . '/.env');

return [
    'providers' => [
        'App\Providers\DatabaseProvider',
        'App\Providers\MailerProvider',
        'App\Providers\ContactProvider'
    ],
    'database' => [
        'driver' => getenv('DB_CONNECTION'),
        'host' => getenv('DB_HOST'),
        'port' => getenv('DB_PORT'),
        'database' => getenv('DB_DATABASE') ?: 'contact',
        'username' => getenv('DB_USERNAME'),
        'password' => getenv('DB_PASSWORD'),
        'table' => getenv('DB_TABLE') ?: 'contact_submissions'
    ],
    'mail' => [
        'driver' => getenv('MAIL_DRIVER'),
        'host' => getenv('MAIL_HOST'),
        'port' => getenv('MAIL_PORT'),
        'username' => getenv('MAIL_USERNAME'),
        'password' => getenv('MAIL_PASSWORD'),
        'to' => getenv('MAIL_TO') ?: 'guy-smiley@example.com'
    ]
];