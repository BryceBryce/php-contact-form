# Bryce's Dealer Inspire PHP Code Challenge
 
## Getting Started


**FIRST! Update database variables in .env with valid MySQL connection information.**

Requires PHP 7+
 
```
cd php-contact-form
composer install
phpunit
php -S 127.0.0.1:9999 -t public
```
 
1. I disabled HTML5 validation to better demonstrate my backend validation.
2. Database and table are created automatically when needed. Not something I would do in production (performance overhead), but keeps you from having to worry about creating them.
3. Created Application class in plain PHP from scratch. 
5. MySQL Database.

## Expectations _CHECK_

Your contact form should be in valid HTML in our template. It should match the style of the template. _CHECK_

Your back-end processing should be done in PHP. You may use a framework, or plain PHP - either is fine. _CHECK_

Your contact form data should be validated. _CHECK_

One copy of the data should be emailed to the owner (listed above).  You can choose either HTML or plaintext email (or a combination). _CHECK_
 
One copy of the data should be kept in a MySQL, MongoDB or Postgres database. _CHECK_

Some indication that the contact form has been sent should be given. _CHECK_

You should have PHPUnit-compatible unit tests for your application. _CHECK_

Provide either a database schema file or a programmatic way of creating your database / tables. _CHECK_
 
The completed work is available in a public git repository for us to checkout and review. _CHECK_